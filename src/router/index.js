import Vue from 'vue'
import VueRouter from 'vue-router'
import Daily from '../views/Daily.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Daily',
    component: Daily
  },
  {
    path: '/annual',
    name: 'Annual',
    component: () => import(/* webpackChunkName: "about" */ '../views/Annual.vue')
  },
  {
    path: '/monthly',
    name: 'Monthly',
    component: () => import(/* webpackChunkName: "about" */ '../views/Monthly.vue')
  },
  {
    path: '/quarterly',
    name: 'Quarterly',
    component: () => import(/* webpackChunkName: "about" */ '../views/Quarterly.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
